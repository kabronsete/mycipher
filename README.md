# Mycipher

This project is a gambas component that provides a complete management of data encryption

This code is a 1.0 version. Betatesters are welcome!!!

Suggestions are welcome

With this gambas component your application will get an complete management of data encryption.
- Function Base64. This function encode/decode a file from/to base64. 
- Function StrBase64. This function encode7decode a string from/to base64.
- Function encode. This function encode/decode a file with symmetric encryption.
- Function StrEncode. This function encode/decode a string with symmetric encryption.
- Function Hencrypt. This function encode a file with hybryd encryption.
- Function Hdecrypt. This function decode a file with hybrid encryption.
- Function SharedSecret. This function creates a Diffie Hellman key.
- Function keyEncrypt. Encrypt a file with a certificate.
- Function KeyDecrypt. Decrypt a file with a private key.
- Function GeneratePassword. This function creates a strong password.

## Getting started

Add this component to your application and instantiate it is the only thing to do. Do it and spend your time on more productive things.

- Read the manual, see the examples and use it.
- Add the component to you application
- Instantiate it
	Dim f As New MyCipher, Response As String 
- Enjoy!!!

 

